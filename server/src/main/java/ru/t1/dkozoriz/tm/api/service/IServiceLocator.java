package ru.t1.dkozoriz.tm.api.service;

import ru.t1.dkozoriz.tm.api.service.dto.ISessionDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.IUserDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.IProjectDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.ITaskDtoService;

public interface IServiceLocator {

    IProjectDtoService getProjectDtoService();

    ITaskDtoService getTaskDtoService();

    IUserDtoService getUserDtoService();

    IAuthService getAuthService();

    IPropertyService getPropertyService();

    ISessionDtoService getSessionDtoService();

    IConnectionService getConnectionService();

}