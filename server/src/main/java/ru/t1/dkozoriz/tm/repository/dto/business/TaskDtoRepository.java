package ru.t1.dkozoriz.tm.repository.dto.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.dto.business.ITaskDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskDtoRepository extends BusinessDtoRepository<TaskDto> implements ITaskDtoRepository {

    public TaskDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<TaskDto> getClazz() {
        return TaskDto.class;
    }

    @Override
    @NotNull
    public List<TaskDto> findAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}