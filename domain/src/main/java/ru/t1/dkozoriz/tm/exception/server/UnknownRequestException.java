package ru.t1.dkozoriz.tm.exception.server;

public final class UnknownRequestException extends AbstractServerException {

    public UnknownRequestException() {
        super("Error! Request is unknown.");
    }

}
